const mqtt = require('mqtt')
const dotenv = require('dotenv')
const express = require('express')
const cors = require('cors')
const bodyParser = require("body-parser")
const http = require('http')
const socketio = require('socket.io')
const pjson = require('./package.json')


dotenv.config()

let available_devices = []

const app = express()
const http_server = http.createServer(app)
const io = socketio(http_server)

app.use(cors())
app.use(bodyParser.json())




const port = process.env.PORT || 80

console.log(`[MQTT] Connecting to ${process.env.MQTT_BROKER_URL}`)
const client = mqtt.connect(
  process.env.MQTT_BROKER_URL,
  {
    username: process.env.MQTT_USERNAME,
    password: process.env.MQTT_PASSWORD
  }
)

client.on('connect', () => {
  console.log(`[MQTT] Connected`)

  // Subscribe to the messages from Pan Tilt platforms
  // Here '+' is for wildcard
  client.subscribe('pantilt/+/status', (err) => {
    if(err) return console.log(err)

  })
})

client.on('error', (error) => {
  console.log(error)
})



client.on('message', (topic, message) => {
  // Deals with MQTT messages from the pantilt platforms
  // message is an object of type Buffer

  let device_id = topic.split('/')[1]

  let message_json = undefined;
  try {
    message_json = JSON.parse(message)
  } catch (e) {
    console.log(`Unable to parse JSON`)
    return
  }

  const find_function = (device) => {return device.id === device_id}

  if(message_json.state === 'connected'){
    if(!available_devices.find(find_function)){
      available_devices.push({
        id: device_id,
        firmware_version: message_json.firmware_version
      })
    }
  }

  if(message_json.state === 'disconnected'){

    let device_index = available_devices.findIndex(find_function)

    if(device_index !== -1){
      available_devices.splice(device_index,1)
    }

  }

  io.emit('status', {id: device_id, ...message_json})

})

// Express
app.get('/', (req, res) => {
  res.send({
    application_name: 'Pan tilt platform server',
    author: 'Maxime MOREILLON',
    mqtt_broker_url:   process.env.MQTT_BROKER_URL || 'Undefined',
    version: pjson.version,
  })
})

app.get('/devices', (req, res) => {
  res.send(available_devices)
})

app.post('/devices/:id/command', (req, res) => {

  const id = req.params.id
  const command = req.body.command

  console.log(`Device ${id} command ${command}`)
  if(command === undefined) {
    console.log(`Undefined command`)
    res.status(400).send(`Undefined position`)
    return
  }

  const topic = `pantilt/${id}/command`
  const payload = JSON.stringify({command: command})
  client.publish(topic, payload)

  res.send('OK')

})

http_server.listen(port, () => {
  console.log(`[Express] Application listening on port ${port}`)
})
